#Retrieve all lectures for a course on coursera
=begin
Instructions:
0. enroll in the course for which you want materials
1. set the directory to where the lectures should be downloaded
2. set your username and password
3. set the index of the course (out of all the courses in which 
   you are enrolled, which number is the course, first = 0)
4. set the number of lectures that need to be downloaded
=end

require "watir-webdriver"
download_directory = ""
coursera_username = ''
coursera_password = ''
course_index = 0
lec_numb = 5

#the following
download_directory.gsub!("/", "\\") if Selenium::WebDriver::Platform.windows?
profile = Selenium::WebDriver::Firefox::Profile.new
profile['browser.download.dir']        = download_directory
profile['browser.download.folderList'] = 2
profile['pdfjs.disabled'] = true
profile['browser.helperApps.neverAsk.saveToDisk'] = "application/pdf"


b = Watir::Browser.new :ff, :profile => profile
b.window.resize_to(1200, 800)
#for some reason going to the main coursera page, then clicking
#on the link for "Sign In" causes the b to crash
b.goto "https://accounts.coursera.org/signin"
b.text_field(:id => 'signin-email').set coursera_username
b.text_field(:id => 'signin-password').set coursera_password
b.button(:type => "submit").click
b.a(:text => /class/).wait_until_present
b.a(:text => /class/, :index => course_index).flash
b.a(:text => /class/, :index => course_index).click
b.a(:text => /Video/).wait_until_present
b.a(:text => /Video/).flash
b.a(:text => /Video/).click
b.a(:text => /PDF/).wait_until_present
for i in (0..(lec_numb-1))
    b.a(:text => /PDF/, :index => i).click
end

#for i in (0..(lec_numb-1))
#    b.a(:text => /PDF/, :index => i).flash
#    #b.a(:text => /PDF/, :index => i).click
#end

#might not want to open a new browser because
#then you have to log in again