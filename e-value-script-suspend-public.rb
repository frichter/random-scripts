#suspend all e-values

evalue_username = ""
evalue_password = ""
evalue_suspend_comment = ""

require "watir-webdriver"
b = Watir::Browser.new :ff
b.window.resize_to(1200, 800)
b.goto "https://www.e-value.net/login.cfm"
b.text_field(:id => 'AIusername').set evalue_username
b.text_field(:id => 'AIpassword').set evalue_password
b.input(:value => 'Login').click
b.goto "https://www.e-value.net/index.cfm?fuseaction=users_evalmodules"
for i in (13..16)
    b.a(:text => /Suspend/, :index => 13).flash
    b.a(:text => /Suspend/, :index => 13).click
    b.text_field(:name => /comment/).set evalue_suspend_comment
    b.input(:value => /Suspend/).click
    puts i
end