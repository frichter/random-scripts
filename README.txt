craigslist-ad.rb
Update a craigslist ad automatically. Originally contracted M&M Abodes.

e-value-script-suspend.rb
Suspend all e-values. DEPRECATED.

e-value-script-fill.rb
Fill out all e-values. DEPRECATED.

pdf_lecs_general.rb
Mine coursera for lectures. Instructions in the file.

secret-santa.py
Generate secret santa matches. Automatically emails gifters their partner