#!/usr/local/bin/python
#SECRET SANTA
#FELIX RICHTER
#MERRY HOLIDAYS

import copy
from random import shuffle

gifters = ["fiona","hope","marcus","felix","vince","nick","seshat",\
    "prashanth","sandhya","cindy","markc","markb"]

email_dict = {"fiona":"fdesland@gmail.com",
    "hope":"hopekronman@gmail.com",
    "marcus":"mabadgeley@gmail.com",
    "felix":"frichter91@gmail.com",
    "vince":"viinsanity@gmail.com",
    "nick":"njheitman89@gmail.com",
    "seshat":"seshatmack@gmail.com",
    "prashanth":"prashanth.raj36@gmail.com",
    "sandhya":"sandchand5@gmail.com",
    "cindy":"ctian1989@gmail.com",
    "markc":"markochen@gmail.com",
    "markb":"marktopia@gmail.com"}

def send_email(gifter, gifter_email, giftee):
    import smtplib

    gmail_user = "frichter91@gmail.com"
    gmail_pwd = ""
    #with 2-step, generate password here:
    #http://support.google.com/accounts/bin/answer.py?answer=185833 
    FROM = 'frichter91@gmail.com'
    TO = [gifter_email] #must be a list
    SUBJECT = "Your secret santa!"
    TEXT = "%s, you've been selected to get a gift for %s! "\
    "Some event details:\n"\
    "Date: Sunday, Dec 14th, 2014\n"\
    "Time: 8pm\n"\
    "Location: Some apartment in Aron\n"\
    "Rules: <$20 / gift\n\nsend bugs to frichter91@gmail.com\n"\
    "source code: https://bitbucket.org/frichter/random-scripts/src/" % (gifter, giftee)

    # Prepare actual message
    message = """\From: %s\nTo: %s\nSubject: %s\n\n%s
    """ % (FROM, ", ".join(TO), SUBJECT, TEXT)
    try:
        #server = smtplib.SMTP(SERVER) 
        server = smtplib.SMTP("smtp.gmail.com", 587) #or port 465 doesn't seem to work!
        server.ehlo()
        server.starttls()
        server.login(gmail_user, gmail_pwd)
        server.sendmail(FROM, TO, message)
        #server.quit()
        server.close()
        print "successfully sent the mail to %s" % (TO)
    except:
        print "failed to send mail to %s" % (TO)

def main():
    giftees = copy.deepcopy(gifters)
    while gifters[-1] == giftees[-1]:
        shuffle(giftees)
    secret_santa = dict()
    out_f = open('secret_santa_master.txt','w')
    for i in range(len(gifters)):
        x = 0
        while gifters[i] == giftees[x]:
            x+=1
        secret_santa[copy.deepcopy(gifters[i])] = copy.deepcopy(giftees[x])
        giftees.pop(x)
    out_f.write("Gifter\tGiftee\n")
    for gifter,giftee in secret_santa.iteritems():
        #print gifter, email_dict[gifter], giftee
        send_email(gifter, email_dict[gifter], giftee)
        out_f.write(gifter+'\t'+giftee+'\n')
    out_f.close()
    #print "secret_santa_master.txt was generated. Use the following awk command to view a single person's giftee:"
    #print "awk 'NR == 1 {print $0} {if ($1 == \"name_here\") print $0}' secret_santa_master.txt"
    #print "replace name_here with one of these names:", gifters

main ()